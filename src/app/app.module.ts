import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { WelcomeModule } from './welcome/welcome.module';
import { LifeCycleModule } from './life-cycle/life-cycle.module';
import { StudentModule } from './student/student.module';
import { CrossComponentModule } from './cross-component/cross-component.module';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    WelcomeModule,
    StudentModule,
    LifeCycleModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    CrossComponentModule,
    RouterModule.forRoot([
      { path: "", redirectTo: "welcome", pathMatch: "full" }
    ])],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
})
export class AppModule { }
