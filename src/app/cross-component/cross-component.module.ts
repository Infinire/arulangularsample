import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeViewComponent } from './employee-view/employee-view.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forRoot([
            { path: "crosscomponent", component: EmployeeComponent }
        ])
    ],
    exports: [EmployeeComponent, EmployeeViewComponent],
    declarations: [EmployeeComponent, EmployeeViewComponent]
})
export class CrossComponentModule {
}