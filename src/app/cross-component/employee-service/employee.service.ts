import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: "root"
})
export class EmployeeService {

    public employeeName=new BehaviorSubject<string>("Arul Alex");
    
    changeEmployeeName(empName:string)    
    {
        this.employeeName.next(empName);
    }
}