import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee-service/employee.service';

@Component({
    selector:"app-employee-view",
    templateUrl:'./employee-view.component.html'
})
export class EmployeeViewComponent implements OnInit{
    
    employeeName:string;
    constructor(private employeeService:EmployeeService){

    }

    ngOnInit(): void {
        this.employeeService.employeeName.subscribe(employeeName=>this.employeeName=employeeName);
    }
    
}