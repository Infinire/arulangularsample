import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee-service/employee.service';

@Component({
    selector:"app-employee",
    templateUrl:'./employee.component.html'
})
export class EmployeeComponent implements OnInit{
    
    employeeName:string;
    constructor(private employeeService:EmployeeService){
    }

    ngOnInit(): void {
        this.employeeService.employeeName.subscribe(employeeName=>this.employeeName=employeeName);
    }

    changeEmployeeName():void{
        this.employeeService.changeEmployeeName(this.employeeName);
    }


}