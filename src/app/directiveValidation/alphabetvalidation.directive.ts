import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[OnlyAlphabets]'
})
export class OnlyAlphabets {

  constructor(private el: ElementRef) { }

  @Input() isAlphabet: boolean;

  @HostListener('keypress', ['$event']) onKeyPress(event: any) {
    let e = <KeyboardEvent>event;
    if (this.isAlphabet) {
      // Ensure that it is a alphabets and allow the keypress
      if ((e.keyCode >= 97 && e.keyCode <= 122) || (e.keyCode >= 65 && e.keyCode <= 90) || e.keyCode == 32) {
        return true;
      }
      else {
        return false;
      }
    }
  }
}