import { NgModule } from '@angular/core';
import { OnlyNumber } from './numbervalidation.directive';
import { OnlyAlphabets } from './alphabetvalidation.directive';

@NgModule({
    imports:[],
    exports:[OnlyNumber,OnlyAlphabets],
    declarations:[OnlyNumber,OnlyAlphabets]
})
export class ValidationModule{

}