import { HttpInterceptor, HttpHandler, HttpRequest } from "@angular/common/http";
import { Observable } from 'rxjs';

export class HeaderInterceptor implements HttpInterceptor{
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
        let newRequest=req.clone({
            setHeaders:{
                'Content-Type':'application/json',
                'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE'
            }
        });
        return next.handle(newRequest);
    }

}