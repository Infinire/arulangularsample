import { Component, OnInit, OnChanges, OnDestroy, Input } from '@angular/core';

@Component({
    selector:'app-life-cycle',
    template:'{{studentName}}'
})
export class LifeCycleComponent implements OnInit,OnChanges,OnDestroy
{
    @Input() studentName:string;
    constructor()
    {
        console.log("Inside the constructor");
    }

    ngOnChanges(): void {
        console.log("On Change Method");
    }
    
    ngOnInit(): void {
        console.log("Init Method");
    }

    ngOnDestroy(): void {
        console.log("Destroy Method");
    }

}