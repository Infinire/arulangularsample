import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { LifeCycleComponent } from './life-cycle.component';
import { LifeCycleLoadComponent } from './life-cycle-load.component';
import { FormsModule } from '@angular/forms';
import { ValidationModule } from '../directiveValidation/directivevaidation.module';

@NgModule({
    imports:[BrowserModule,FormsModule,
            ValidationModule,
            RouterModule.forRoot([
            { path:"lifecycle", component:LifeCycleLoadComponent }
    ])],
    declarations:[LifeCycleLoadComponent,LifeCycleComponent],
    exports:[LifeCycleLoadComponent]
})
export class LifeCycleModule{

}