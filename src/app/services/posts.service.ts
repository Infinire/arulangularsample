import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap,catchError } from 'rxjs/operators';
import { jsonpCallbackContext } from '@angular/common/http/src/module';

@Injectable({
  providedIn: "root"
})
export class PostsService {

  postsUrl: string = "https://jsonplaceholder.typicode.com/users";

  constructor(private http: HttpClient) { }

  getPostsDetails(): Observable<any[]> {
    return this.http.get<any[]>(this.postsUrl).pipe(catchError(this.handleError));
  }

  private handleError(err:HttpErrorResponse)
  {
    return throwError(err.message);
  }
  
}