import { Injectable } from '@angular/core';
import { IStudent } from '../student/student-list/student';
import { IStudentProfile } from '../student/student-profile/studentprofile';
import { Observable, throwError, interval } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { StudentModel, CityModel } from '../student/student-list/student-model';
import './service-url'
import { studentAPIUrl } from './service-url';
@Injectable({
  providedIn: "root"
})
export class StudentService {

  studentProfile: IStudentProfile[];

  constructor(private http: HttpClient) { }

  getStudentList() {
    return [{
      firstName: "Arul Alex",
      lastName: "Raj C",
      gender: "Male",
      mobileNo: "8760797897"
    },
    {
      firstName: "Amal",
      lastName: "Raj ",
      gender: "Male",
      mobileNo: "8838925916"
    },
    {
      firstName: "Aswin",
      lastName: "Prathap ",
      gender: "Male",
      mobileNo: "9856743214"
    },
    {
      firstName: "Francis",
      lastName: "Xavier ",
      gender: "Male",
      mobileNo: "9856743214"
    }];
  }

  performFilter(value: string, studentList: IStudent[]): IStudent[] {
    return studentList.filter((studentList: IStudent) => studentList.firstName.toLocaleLowerCase().includes(value.toLocaleLowerCase()));
  }

  getStudentProfile(): Promise<IStudentProfile[]> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.http.get<IStudentProfile[]>(studentAPIUrl + "/student").toPromise());
      }, 2000);
    });
  }

  getStudentProfileById(studentId: number): Observable<StudentModel> {
    return this.http.get<StudentModel>(studentAPIUrl + "/student/" + studentId).pipe(catchError(this.handleException));
  }

  getCity(): Promise<CityModel[]> {
    return this.http.get<CityModel[]>(studentAPIUrl + "/city").toPromise();
  }

  private handleException(err: HttpErrorResponse) {
    return throwError(err.message);
  }

  saveStudentProfile(studentProfile: StudentModel): Observable<StudentModel> {
    return this.http.post<StudentModel>(studentAPIUrl + "/Student", JSON.stringify(studentProfile)).
      pipe(catchError(this.handleException));
  }

  updateStudentProfile(id: number, studentProfile: StudentModel): Observable<string> {
    return this.http.put<string>(studentAPIUrl + "/student/" + id, JSON.stringify(studentProfile)).
      pipe(catchError(this.handleException));
  }

  deleteStudentProfile(id: number): Observable<object> {
    return this.http.delete(studentAPIUrl + "/student/" + id).
      pipe(catchError(this.handleException));
  }

}