import { CommonMessage } from '../shared/common';
import { ToastrService } from 'ngx-toastr'
import { Injectable } from '@angular/core';

@Injectable({
    providedIn:"root"
})
export class ToastrMessageService{

    constructor(private toaster:ToastrService){

    }

    ShowMessage(commonMessage:CommonMessage,message:string):void {
        switch(commonMessage)
        {
            case CommonMessage.success:
                this.toaster.success(message);
            break;

            case CommonMessage.info:
                this.toaster.info(message);
            break;

            case CommonMessage.fail:
                this.toaster.error(message);
            break;

            case CommonMessage.warning:
                this.toaster.warning(message);
            break;
        }
    }
}