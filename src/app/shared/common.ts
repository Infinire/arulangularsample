export enum CommonMessage
{
    success,
    info,
    warning,
    fail,
}