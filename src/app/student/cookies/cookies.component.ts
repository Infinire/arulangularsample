import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: "app-cookies",
    templateUrl: "./cookies.component.html"
})
export class CookiesComponent implements OnInit {

    studentName:string;
    cookieValues:string;

    constructor(private cookies: CookieService) { }

    ngOnInit(): void {
        this.cookieValues=this.cookies.get("StudentName");
    }

    setCookieValues():void{
        this.cookies.set("StudentName",this.studentName);
        this.cookieValues=this.cookies.get("StudentName");
    }

    clearCookieValues():void{
        this.cookies.delete("StudentName");
        this.cookieValues=this.cookies.get("StudentName");
    }

}