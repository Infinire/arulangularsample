import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CookiesComponent } from './cookies.component';
import { CookieService } from 'ngx-cookie-service';
import { ValidationModule } from 'src/app/directiveValidation/directivevaidation.module';

@NgModule({
    imports:[
        BrowserModule,
        FormsModule,
        ValidationModule
    ],
    exports:[CookiesComponent],
    declarations:[CookiesComponent],
    providers:[CookieService]
})
export class CookiesModule{

}