import { Component, OnInit } from '@angular/core';

@Component({
    selector: "app-local-storage",
    templateUrl: './local-storage.component.html'
})
export class LocalStorageComponent implements OnInit {
    studentName: string;
    localValue: string;

    ngOnInit(): void {
        this.localValue = localStorage.getItem("Name");
    }

    addLocalStorageValue(): void {
        localStorage.setItem("Name", this.studentName);
        this.localValue = localStorage.getItem("Name");
    }

    clearLocalStorageValue():void{
        localStorage.removeItem("Name"); // Remove particular item
        //localStorage.clear(); // Clear all local storage value
        this.localValue = localStorage.getItem("Name");
    }

}