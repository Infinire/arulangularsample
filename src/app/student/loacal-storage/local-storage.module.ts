import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { LocalStorageComponent } from './local-storage.component';
import { ValidationModule } from 'src/app/directiveValidation/directivevaidation.module';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ValidationModule
    ],
    exports: [LocalStorageComponent],
    declarations: [LocalStorageComponent]
})
export class LocalStorageModule { }