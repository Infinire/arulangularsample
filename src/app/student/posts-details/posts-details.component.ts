import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-posts-details',
  templateUrl: './posts-details.component.html',
  styleUrls: ['./posts-details.component.css']
})
export class PostsDetailsComponent implements OnInit {

  postsDetails:any[];

  constructor(private postsService:PostsService)
  { }

  ngOnInit() {
    this.postsService.getPostsDetails().subscribe(data=>this.postsDetails=data,error=>console.log(error));
  }

}