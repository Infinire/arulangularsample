import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PostsDetailsComponent } from './posts-details.component';

@NgModule({
    imports:[BrowserModule],
    exports:[PostsDetailsComponent],
    declarations:[PostsDetailsComponent],
    bootstrap:[PostsDetailsComponent],
})
export class PostsModule{

}