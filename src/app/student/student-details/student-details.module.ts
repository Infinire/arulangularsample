import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StudentDetailsComponent } from './student-details';

@NgModule({
    imports:[BrowserModule],
    exports:[StudentDetailsComponent],
    declarations:[StudentDetailsComponent]
})
export class StudentDetailsModule{

}