import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: "app-student-details",
  templateUrl: "./student-details.html"
})

export class StudentDetailsComponent {
  @Input() StudentName: string;
  @Output() nameClicked: EventEmitter<string>=new EventEmitter<string>();
  

  onClick():void{
    this.nameClicked.emit(`Selected Student Name is (${this.StudentName})`);
  }
}