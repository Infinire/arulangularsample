import { Component, OnInit } from '@angular/core';
import { IStudent } from './student';
import { StudentModel } from './student-model';
import { StudentService } from '../../services/student.service';
@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styles: ['thead{color:#8ad3ff;} tbody{ color: #3dc1a0!important;} ']
})
export class StudentListComponent implements OnInit {

  isAdd: boolean = false;
  isView: boolean = true;
  studentData: StudentModel = new StudentModel();
  studentList: IStudent[];
  tempStudentList: IStudent[];

  showStudentList(): void {
    this.showStudentView();
  }

  addNewStudent(): void {
    this.showAddView();
    this.clearValues();
  }

  saveStudentDetails(): void {
    this.tempStudentList.push({
      firstName: this.studentData.firstName,
      lastName: this.studentData.lastName,
      gender: (this.studentData.gender) == "1" ? "Male" : "Female",
      mobileNo: this.studentData.mobileNo
    });
    this.studentList = this.tempStudentList;
    this.showStudentView();
  }

  clearValues(): void {
    this.studentData = null;
    this.studentData = new StudentModel();
  }

  showStudentView(): void {
    this.isView = true;
    this.isAdd = false;
  }

  showAddView(): void {
    this.isView = false;
    this.isAdd = true;
  }

  set getFilterValue(filterValue: string) {
    this.studentList = filterValue != "" ? this.studentService.performFilter(filterValue, this.tempStudentList) : this.tempStudentList;
  }

  onNameClicked(message: string): void {
    this.studentData.pageTitle = message;
  }

  constructor(private studentService: StudentService) {
  }

  ngOnInit(): void {
    this.studentList = this.tempStudentList = this.studentService.getStudentList();
  }

}