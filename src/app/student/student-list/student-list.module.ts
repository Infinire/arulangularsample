import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { StudentListComponent } from './student-list.component';
import { StudentDetailsModule } from '../student-details/student-details.module';
import { FormsModule } from '@angular/forms';
import { ValidationModule } from '../../directiveValidation/directivevaidation.module';

@NgModule({
    imports:[BrowserModule,FormsModule,
            StudentDetailsModule,
            ValidationModule,
            RouterModule],
    exports:[StudentListComponent],
    declarations:[StudentListComponent]
})
export class StudentListModule{

}