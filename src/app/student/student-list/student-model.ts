export class StudentBasicModel {
  studentId: number;
  city: number;
  email: string;
  address: string;
}

export class StudentModel extends StudentBasicModel {
  pageTitle: string = "Student Details";
  firstName: string;
  lastName: string;
  gender: string;
  mobileNo: string;
  filterValue: string;
  errorMessage: string;
}

export class GenderModel {
  genderId: number;
  gender: string;
}

export class CityModel {
  cityId: number;
  city: string;
}