export interface IStudent {
    firstName: string;
    lastName: string;
    gender: string;
    mobileNo: string;
  }