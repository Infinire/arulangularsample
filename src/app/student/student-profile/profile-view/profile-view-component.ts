import { Input, OnInit, OnDestroy, OnChanges, Component, Output, EventEmitter } from '@angular/core';
import { StudentModel } from 'src/app/student/student-list/student-model';
import { ActivatedRoute } from '@angular/router';
import { StudentService } from 'src/app/services/student.service';

@Component({
    selector: "profile-view",
    templateUrl: "./profile-view.component.html"
})
export class ProfileViewComponent implements OnInit, OnDestroy, OnChanges {

    studentData: StudentModel;
    @Output() isProfileView: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(private routes: ActivatedRoute, private studentService: StudentService) {
        console.log("Load Constructor");
    }

    ngOnInit(): void {
        try {
            this.loadProfileView(parseInt(this.routes.snapshot.params["id"]));
        } catch (error) {
            console.log(error.message);
        }
        console.log("Load Init Method");
    }

    ngOnChanges(): void {
        console.log("Load On Change Method")
    }

    ngOnDestroy(): void {
        console.log("Load Destory Method");
    }

    onBackClick(): void {
        this.isProfileView.emit(true);
    }

    loadProfileView(studentId: number): void {
        this.studentService.getStudentProfileById(studentId).subscribe(data => {
            this.studentData = data;
        }, error => console.log(error));
    }
}