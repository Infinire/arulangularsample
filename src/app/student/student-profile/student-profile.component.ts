import { Component, OnInit, OnDestroy } from '@angular/core';
import { IStudentProfile } from './studentprofile';
import { StudentModel, GenderModel, CityModel } from 'src/app/student/student-list/student-model';
import { StudentService } from 'src/app/services/student.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrMessageService } from 'src/app/services/toastr-service';
import { CommonMessage } from 'src/app/shared/common';
import { successValue } from 'src/app/services/service-url';
import { Subscription } from 'rxjs';

@Component({
    selector: "app-student-profile",
    templateUrl: "./student-profile.component.html",
    styles: ['thead{color:#8ad3ff;}']
})
export class StudentProfileComponent implements OnInit, OnDestroy {

    isView: boolean;
    isAdd: boolean;
    isProfileView: boolean;
    isInValid: boolean;
    studentProfile: IStudentProfile[];
    filteredStudent: IStudentProfile[];
    studentData: StudentModel;
    cityDetails: CityModel[];
    genderDetails: GenderModel[];
    display: string = "none";
    errorMessage: string;
    filterByCity: number = 0;
    errors: string[];

    subGetStudentById: Subscription;
    subSaveStudent: Subscription;
    subUpdateStudent: Subscription;
    subDeleteStudent: Subscription;

    constructor(private studentService: StudentService, private route: ActivatedRoute,
        private messageService: ToastrMessageService) {
    }

    ngOnInit(): void {
        this.viewStudentProfile();
        this.getCity();
    }

    addStudentProfile(): void {
        this.studentData = new StudentModel();
        this.studentData.city = 1;
        this.studentData.gender = "1";
        this.errors = [];
        this.isView = this.isProfileView = this.isInValid = false;
        this.isAdd = true;
    }

    editStudentProfile(studentId: number): void {
        this.addStudentProfile();
        this.subGetStudentById = this.studentService.getStudentProfileById(studentId).subscribe(data => {
            this.studentData = data;
            this.studentData.gender = this.studentData.gender.toString();// since the value of the radio button is string so converting it as string
        }, error => {
            this.showPopup();
            this.errorMessage = "Error accured contact Administrator...";
        });
    }

    viewStudentProfile(): void {
        this.getStudentProfile();
        this.isView = true;
        this.isAdd = this.isProfileView = false;
    }

    async getStudentProfile() {
        try {
            this.studentProfile = null;
            let studentById = await this.studentService.getStudentProfile();
            this.studentProfile = this.filteredStudent = <IStudentProfile[]>studentById;
            console.log("Calls after await");
        } catch (error) {
            this.showPopup();
            this.errorMessage = "Unable to load the student profile, something went wrong!!!.....";
        }
    }

    getCity(): void {
        this.studentService.getCity().then(data => {
            this.cityDetails = data;
        }).catch(error => {
            this.showPopup();
            this.errorMessage = "Error accured contact Administrator...";
        });
    }

    saveStudentProfile(): void {
        if (this.validateForm()) {
            if (this.studentData.studentId == null) {
                this.subSaveStudent = this.studentService.saveStudentProfile(this.studentData).subscribe(data => {
                    if (data.studentId != null) {
                        this.viewStudentProfile();
                        this.messageService.ShowMessage(CommonMessage.success, 'Record has been saved successfully');
                    } else {
                        this.messageService.ShowMessage(CommonMessage.warning, 'Sorry! Unable to save the record');
                    }
                }, error => {
                    this.showPopup();
                    this.errorMessage = "Sorry! error accured, Unable to save the record";
                });
            } else {
                this.subUpdateStudent = this.studentService.updateStudentProfile(this.studentData.studentId, this.studentData).subscribe(data => {
                    if (data == successValue) {
                        this.messageService.ShowMessage(CommonMessage.info, 'Record has been updated successfully');
                    } else {
                        this.messageService.ShowMessage(CommonMessage.warning, 'Sorry! Unable to update the record');
                    }
                }, error => {
                    this.showPopup();
                    this.errorMessage = "Sorry! error accured, Unable to update the record";
                });
            }
        }
    }

    deleteStudentProfile(studentId: number): void {
        if (confirm('Are you sure to delete the student profile?')) {
            this.subDeleteStudent = this.studentService.deleteStudentProfile(studentId).subscribe(data => {
                this.messageService.ShowMessage(CommonMessage.success, 'Record has been deleted successfully');
                this.getStudentProfile();
            }, error => {
                this.showPopup();
                this.errorMessage = "Sorry! error accured, Unable to delete the record";
            });
        }
    }

    changeStatus(isProfileView: boolean): void {
        this.isProfileView = isProfileView;
    }

    ngOnDestroy(): void {
        if (this.subGetStudentById != null) {
            this.subGetStudentById.unsubscribe();
        }
        if (this.subSaveStudent != null) {
            this.subSaveStudent.unsubscribe();
        }
        if (this.subUpdateStudent != null) {
            this.subUpdateStudent.unsubscribe();
        }
        if (this.subDeleteStudent != null) {
            this.subDeleteStudent.unsubscribe();
        }
    }

    showPopup(): void {
        this.display = "block";
    }

    closePopup(): void {
        this.display = "none";
    }

    filterStudentByCity(): void {
        this.filteredStudent = this.filterByCity == 0 ? this.studentProfile : this.studentProfile.filter((studentProfile: IStudentProfile) => studentProfile.city == this.filterByCity);
    }

    validateForm(): boolean {
        this.errors = [];
        this.isInValid = false;
        if (this.studentData.firstName == null || this.studentData.firstName == "") {
            this.errors.push("Enter first name");
            this.isInValid = true;
        }
        if (this.studentData.lastName == null || this.studentData.lastName == "") {
            this.errors.push("Enter last name");
            this.isInValid = true;
        }
        if (this.studentData.mobileNo == null || this.studentData.mobileNo == "") {
            this.errors.push("Enter mobile no");
            this.isInValid = true;
        }
        if (this.studentData.mobileNo != "") {
            if (this.studentData.mobileNo != null && this.studentData.mobileNo.length != 10) {
                this.errors.push("Enter valid mobile no");
                this.isInValid = true;
            }
        }
        if (this.studentData.email == null || this.studentData.email == "") {
            this.errors.push("Enter email id");
            this.isInValid = true;
        }
        if (this.studentData.email != null && this.studentData.email != "" && !RegExp("[^@]+@[^\.]+\.[a-z]{2,3}").test(this.studentData.email)) {
            this.errors.push("Enter valid email id");
            this.isInValid = true;
        }
        if (this.studentData.address == null || this.studentData.address == "") {
            this.errors.push("Enter address");
            this.isInValid = true;
        }
        return !this.isInValid;
    }

    clearValidation(): void {
        this.isInValid = true;
        if (this.errors != null) {
            if (this.studentData.firstName != null && this.studentData.firstName != "") {
                this.errors = this.errors.filter((value: string) => value != "Enter first name");
            }
            if (this.studentData.lastName != null && this.studentData.lastName != "") {
                this.errors = this.errors.filter((value: string) => value != "Enter last name");
            }
            if (this.studentData.mobileNo != null && this.studentData.mobileNo != "") {
                this.errors = this.errors.filter((value: string) => value != "Enter mobile no");
                if (this.studentData.mobileNo != null && this.studentData.mobileNo.length == 10) {
                    this.errors = this.errors.filter((value: string) => value != "Enter valid mobile no");
                }
            }
            if (this.studentData.email != null && this.studentData.email != "") {
                this.errors = this.errors.filter((value: string) => value != "Enter email id");
            }
            if (this.studentData.email != null && this.studentData.email != "" && RegExp("[^@]+@[^\.]+\.[a-z]{2,3}").test(this.studentData.email)) {
                this.errors = this.errors.filter((value: string) => value != "Enter valid email id");
            }
            if (this.studentData.address != null && this.studentData.address != "") {
                this.errors = this.errors.filter((value: string) => value != "Enter address");
            }
        }
    }

}