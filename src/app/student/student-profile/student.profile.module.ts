import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { StudentProfileComponent } from './student-profile.component';
import { ValidationModule } from 'src/app/directiveValidation/directivevaidation.module';
import { ProfileViewComponent } from './profile-view/profile-view-component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ValidationModule,
        RouterModule.forRoot([
            { path: "profileview/:id", component: ProfileViewComponent },
          ])
    ],
    declarations: [StudentProfileComponent, ProfileViewComponent],
    exports: [StudentProfileComponent]
})
export class StudentProfileModule {

}