import { IStudent } from 'src/app/student/student-list/student';

export interface IStudentProfile{
    firstName: string;
    lastName: string;
    gender: number;
    mobileNo: string;
    email: string;
    city:number;
    address:string;
}