import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-student-view',
  templateUrl: './student-view.component.html',
  styleUrls: ['./student-view.component.css']
})
export class StudentViewComponent implements OnInit {

  constructor(private route:ActivatedRoute) { }
  Title:string="Student View";
  Name:string;

  ngOnInit() {
    this.Name=" You have seleted : "+this.route.snapshot.paramMap.get("Name");
  }

}