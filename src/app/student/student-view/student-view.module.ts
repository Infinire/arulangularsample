import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { StudentViewComponent } from './student-view.component';

@NgModule({
    imports:[BrowserModule,
        FormsModule,
        RouterModule.forRoot([
            { path: "studentview/:Name", component: StudentViewComponent },
          ])],
    exports:[StudentViewComponent],
    declarations:[StudentViewComponent]
})
export class StudentViewModule{}