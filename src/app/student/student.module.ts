import { NgModule } from '@angular/core';
import { StudentListModule } from './student-list/student-list.module';
import { PostsModule } from './posts-details/posts-details.module';
import { StudentProfileModule } from './student-profile/student.profile.module';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { StudentComponent } from './student.component';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { PostsDetailsComponent } from './posts-details/posts-details.component';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentViewModule } from './student-view/student-view.module';
import { ProfileViewComponent } from './student-profile/profile-view/profile-view-component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeaderInterceptor } from '../headers.interceptors';
import { LocalStorageModule } from './loacal-storage/local-storage.module';
import { LocalStorageComponent } from './loacal-storage/local-storage.component';
import { CookiesModule } from './cookies/cookies.module';
import { CookiesComponent } from './cookies/cookies.component';

@NgModule({
    imports: [FormsModule,
        BrowserModule,
        StudentListModule,
        PostsModule,
        StudentProfileModule,
        StudentViewModule,
        LocalStorageModule,
        CookiesModule,
        RouterModule.forRoot([
            { path: "student", component: StudentComponent },
        ]),
        RouterModule.forChild([
            {
                path: "student", component: StudentComponent, children: [
                    { path: '', redirectTo: "studentlist", pathMatch: "full" },
                    { path: 'studentprofile', component: StudentProfileComponent },
                    { path: 'postdetails', component: PostsDetailsComponent },
                    { path: 'studentlist', component: StudentListComponent },
                    { path: 'profileview/:id', component: ProfileViewComponent },
                    { path: 'localstorage', component: LocalStorageComponent },
                    { path: 'cookies', component: CookiesComponent }
                ]
            }
        ])
    ],
    exports: [StudentComponent],
    declarations: [StudentComponent],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true }
    ]
})
export class StudentModule {

}