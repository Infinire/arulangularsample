import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { WelcomeComponent } from './welcome.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports:[BrowserModule,
        RouterModule.forRoot([
        { path: "welcome", component: WelcomeComponent }
      ])],
    exports:[WelcomeComponent],
    declarations:[WelcomeComponent],
})
export class WelcomeModule{

}